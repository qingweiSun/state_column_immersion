package com.sun.statusbar

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ivTop.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }

    }
}
