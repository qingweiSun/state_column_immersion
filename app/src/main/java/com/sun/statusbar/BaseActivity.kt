package com.sun.statusbar

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sun.statusbarutil.StatusBarUtil

abstract class BaseActivity : AppCompatActivity() {
    var statusBar: StatusBarUtil? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusBar = StatusBarUtil.get(this)
        statusBar?.let {
            it.statusBarImage()
        }
    }


}