package com.sun.statusbar

import android.os.Bundle
import android.view.WindowManager

class SecondActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusBar?.statusBarColor(resources.getColor(R.color.colorPrimary), 100)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        setContentView(R.layout.second_layout)

    }
}
