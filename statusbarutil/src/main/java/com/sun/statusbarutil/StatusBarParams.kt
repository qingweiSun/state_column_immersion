package com.sun.statusbarutil

import android.graphics.Color

class StatusBarParams {
    var color: Int = Color.parseColor("#000000")//默认颜色黑色
    var translucent: Int = 0//透明度
    var dark: Boolean = false//透明度
    var type: String = StatusBarUtil.STATUS_COLOR
}