package com.sun.statusbarutil

class StatusBarException(msg: String) : Exception(msg)