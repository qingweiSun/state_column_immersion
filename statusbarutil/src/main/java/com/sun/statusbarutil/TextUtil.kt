package com.sun.statusbarutil

import androidx.annotation.Nullable

class TextUtil {


    companion object {

        /**
         * Returns true if the string is null or 0-length.
         *
         * @param str the string to be examined
         * @return true if str is null or zero length
         */
        fun isEmpty(@Nullable str: CharSequence?): Boolean {
            return if (str == null || str.length == 0 || "null" == str)
                true
            else
                false
        }

        /***
         * 字符串对比
         * @param str1
         * @param str2
         * @return
         */
        fun equals(str1: String, str2: String): Boolean {
            return if (isEmpty(str1) || isEmpty(str2)) {
                false
            } else {
                str1 == str2
            }
        }
    }
}