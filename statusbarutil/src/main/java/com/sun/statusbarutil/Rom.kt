package com.sun.statusbarutil

import android.os.Build


import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class Rom {


    companion object {
        internal val ROM_MIUI = "MIUI"
        internal val ROM_EMUI = "EMUI"
        internal val ROM_FLYME = "FLYME"
        internal val ROM_OPPO = "OPPO"
        internal val ROM_SMARTISAN = "SMARTISAN"
        internal val ROM_VIVO = "VIVO"
        internal val ROM_QIKU = "QIKU"
        internal val KEY_VERSION_MIUI = "ro.miui.ui.version.name"
        internal val KEY_VERSION_EMUI = "ro.build.version.emui"
        internal val KEY_VERSION_OPPO = "ro.build.version.opporom"
        internal val KEY_VERSION_SMARTISAN = "ro.smartisan.version"
        internal val KEY_VERSION_VIVO = "ro.vivo.os.version"

        val isEmui: Boolean
            get() = check(ROM_EMUI)

        val isMiui: Boolean
            get() = check(ROM_MIUI)

        val isVivo: Boolean
            get() = check(ROM_VIVO)

        val isOppo: Boolean
            get() = check(ROM_OPPO)

        val isFlyme: Boolean
            get() = check(ROM_FLYME)

        val is360: Boolean
            get() = check(ROM_QIKU) || check("360")

        val isSmartisan: Boolean
            get() = check(ROM_SMARTISAN)

        val name: String?
            get() {
                if (sName == null) {
                    check("")
                }
                return sName
            }

        val version: String?
            get() {
                if (sVersion == null) {
                    check("")
                }
                return sVersion
            }

        /***
         * 系统版本号>=6.0
         */
        val isM: Boolean
            get() {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    true
                } else {
                    false
                }
            }
        /***
         * 系统版本>=5.0
         */
        val isL: Boolean
            get() {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    true
                } else {
                    false
                }
            }

        /***
         * 系统版本>=4.4
         */
        val isKt: Boolean
            get() {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    true
                } else {
                    false
                }
            }


        internal var sName: String? = null
        internal var sVersion: String? = null


        internal fun check(rom: String): Boolean {

            if (sName != null) {
                return sName == rom
            }

            if (!TextUtil.isEmpty(getProp(KEY_VERSION_MIUI))) {
                sName = ROM_MIUI
            } else if (!TextUtil.isEmpty(getProp(KEY_VERSION_EMUI))) {
                sName = ROM_EMUI
            } else if (!TextUtil.isEmpty(getProp(KEY_VERSION_OPPO))) {
                sName = ROM_OPPO
            } else if (!TextUtil.isEmpty(getProp(KEY_VERSION_VIVO))) {
                sName = ROM_VIVO
            } else if (!TextUtil.isEmpty(getProp(KEY_VERSION_SMARTISAN))) {
                sName = ROM_SMARTISAN
            } else {
                sVersion = Build.DISPLAY
                if (sVersion!!.toUpperCase().contains(ROM_FLYME)) {
                    sName = ROM_FLYME
                } else {
                    sVersion = Build.UNKNOWN
                    sName = Build.MANUFACTURER.toUpperCase()
                }
            }
            return sName == rom
        }

        internal fun getProp(name: String): String? {
            var input: BufferedReader? = null
            try {
                val p = Runtime.getRuntime().exec("getprop $name")
                input = BufferedReader(InputStreamReader(p.inputStream), 1024)
                sVersion = input.readLine()
                input.close()
            } catch (ex: IOException) {
                sVersion = null
            } finally {
                if (input != null) {
                    try {
                        input.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
            return sVersion
        }
    }
}
