package com.sun.statusbarutil

import androidx.annotation.ColorInt

class ColorUtil {

    //判断给定的颜色值是否合法

    companion object {
        fun checkColor(color: String): Boolean {
            val regex = "^#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})$"
            return color.matches(regex.toRegex())
        }

        /**
         * 计算状态栏颜色明暗度
         *
         * @param color color值
         * @param alpha alpha值
         * @return 最终的状态栏颜色
         */
        fun statusColorIntensity(@ColorInt color: Int, alpha: Int): Int {
            if (alpha == 0) {
                return color
            }
            val a = 1 - alpha / 255f
            var red = color shr 16 and 0xff
            var green = color shr 8 and 0xff
            var blue = color and 0xff
            red = (red * a + 0.5).toInt()
            green = (green * a + 0.5).toInt()
            blue = (blue * a + 0.5).toInt()
            return 0xff shl 24 or (red shl 16) or (green shl 8) or blue
        }

    }
}