package com.sun.statusbarutil

import android.annotation.TargetApi
import android.app.Activity
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.RequiresApi


class StatusBarUtil(activity: Activity) {

    private var statusBarParams: StatusBarParams = StatusBarParams()
    internal var window: Window? = null

    init {
        window = activity.window
    }

    companion object {

        val STATUS_COLOR = "STATUS_COLOR"
        val STATUS_IMAGE = "STATUS_IMAGE"

        fun get(activity: Activity): StatusBarUtil {
            if (activity == null) {
                throw NullPointerException("activity 不能为空")
            } else {
                return StatusBarUtil(activity)
            }
        }
    }

    /***
     * 顶部不显示颜色,显示图片
     */
    fun statusBarImage() {
        statusBarParams.type = STATUS_IMAGE
        load()
    }

    /***
     * 设置状态栏背景色
     * @param color 颜色
     */
    fun statusBarColor(color: Int) {
        statusBarParams.color = color
        statusBarParams.type = STATUS_COLOR
        load()
    }

    /***
     * 设置状态栏背景色
     * @param color 颜色
     * @param translucent 透明度
     */
    fun statusBarColor(color: Int, translucent: Int) {
        if (translucent < 0 || translucent > 255) {
            throw StatusBarException("透明度不合法,请在0到255之间")
        } else {
            statusBarParams.type = STATUS_COLOR
            statusBarParams.color = color
            statusBarParams.translucent = translucent
            load()
        }
    }

    /***
     * 设置状态栏背景色
     * @param color 颜色
     * @param translucent 透明度
     * @param dark  Android 6.0+ 状态栏图标原生反色操作
     */
    fun statusBarColor(color: Int, translucent: Int, dark: Boolean = true) {
        if (translucent < 0 || translucent > 255) {
            throw StatusBarException("透明度不合法,请在0到255之间")
        } else {
            statusBarParams.type = STATUS_COLOR
            statusBarParams.color = color
            statusBarParams.translucent = translucent
            statusBarParams.dark = dark
            load()
        }
    }

    /***
     * 最后的执行
     */
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun load() {
        if (Rom.isKt) { //>=4.4
            if (Rom.isM) { //>=6.0
                val decorView = window?.getDecorView()
                decorView?.let {
                    var vis = it.getSystemUiVisibility()
                    if (statusBarParams.dark) {
                        vis = vis or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    } else {
                        vis = vis and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                    }
                    it.setSystemUiVisibility(vis)
                }

                setApi {
                    setDefult(it)
                }
            } else if (Rom.isL) {
                if (Rom.isMiui) {
                    setApi {
                        setMiui(statusBarParams.dark, it)
                    }
                } else if (statusBarParams.translucent == 0 && statusBarParams.dark == true) {
                    //不支持反色,但是设置了反色,则添加透明度,保证能看见字
                    statusBarParams.translucent = 127;
                    setApi {
                        setDefult(it)
                    }
                }
            } else {
                setApi {
                    setDefult(it)
                }
            }
        }
    }

    /***
     * 设置miui
     */
    private fun setMiui(darkmode: Boolean, window: Window) {
        val clazz = window.javaClass
        try {
            var darkModeFlag = 0
            val layoutParams = Class.forName("android.view.MiuiWindowManager\$LayoutParams")
            val field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE")
            darkModeFlag = field.getInt(layoutParams)
            val extraFlagField = clazz.getMethod("setExtraFlags", Int::class.javaPrimitiveType, Int::class.javaPrimitiveType)
            extraFlagField.invoke(window, if (darkmode) darkModeFlag else 0, darkModeFlag)
            if (darkmode) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.getDecorView()?.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
            } else {
                val flag = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                window.getDecorView()?.systemUiVisibility = flag
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /***
     * 设置原生手机
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setDefult(window: Window) {
        if (TextUtil.equals(STATUS_COLOR, statusBarParams.type)) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(ColorUtil.statusColorIntensity(statusBarParams.color, statusBarParams.translucent))
        } else {
            //透明状态栏
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * 在 Kotlin 中无返回为 Unit
     * 此方法接收一个无参数的函数并且无返回
     * 使用参数名加 () 来调用
     */
    private fun setApi(api: (window: Window) -> Unit) {
        window?.let {
            api(it)
        }
    }


}